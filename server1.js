//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

app.listen(port);
console.log('todo list RESTful API server started on: ' + port);
var bodyparser =require ('body-parser');
app.use(bodyparser.json());

var movimientosJSON=require('./movimientosv2.json');



app.get('/',function(req, res){
  //res.send("hola mundo nodejs");
  res.sendfile(path.join(__dirname,'index.html'));
});

app.get('/clientes',function(req, res){
  res.send("Aqui están los clientes devueltos");
});

app.get('/clientes/:idcliente',function(req, res){
  res.send("Aqui tiene al cliente: " + req.params.idcliente);
});

app.get('/movimientos/v1',function(req, res){
  //res.send("hola mundo nodejs");
  res.sendfile('movimientos.json');
});

app.get('/movimientos/v2',function(req, res){
  //res.send("hola mundo nodejs");
  //console.log(req.query)
  res.send(movimientosjson);
});

app.get('/movimientos/v2/:index',function(req, res){
  //res.send("hola mundo nodejs");
  console.log(req.params.index)
  res.send(movimientosjson[req.params.index]);
});

app.post('/',function(req, res){
  res.send("Hemos recibido una petición post");
});

app.post('/movimientos/v2',function(req, res){
  var nuevo=req.body
  nuevo.id =movimientosJSON.length + 1;
  movimientosJSON.push(nuevo);
  res.send('movimiento dado de alta');
});
